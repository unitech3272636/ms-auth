package az.unitech.msauth.service;

import az.unitech.msauth.dao.entity.UserEntity;
import az.unitech.msauth.dto.UserDto;
import az.unitech.msauth.dto.request.LoginRequest;
import az.unitech.msauth.dto.request.RegistrationRequest;
import az.unitech.msauth.dto.response.LogInResponse;
import az.unitech.msauth.dto.response.RegistrationResponse;
import az.unitech.msauth.enums.BusinessResultEnum;
import az.unitech.msauth.exception.CustomException;
import az.unitech.msauth.util.JwtUtil;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

    @InjectMocks
    private AuthService authService;
    @Mock
    private UserService userService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtUtil jwtUtil;

    @Test
    public void testLoginSuccess() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest("testuser", "password");
        UserDto userDto = UserDto.builder().id(1L).username("testuser").password("hashedPassword").build();

        when(userService.findByUserName(loginRequest.getUsername()))
                .thenReturn(Optional.of(userDto));
        when(passwordEncoder.matches(loginRequest.getPassword(), userDto.getPassword()))
                .thenReturn(true);
        when(jwtUtil.generateToken(userDto))
                .thenReturn("fakeAccessToken");

        // Act
        LogInResponse response = authService.login(loginRequest);

        // Assert
        assertNotNull(response);
        assertEquals("fakeAccessToken", response.getAccessToken());

        // Verify that the methods were called
        verify(userService).findByUserName(loginRequest.getUsername());
        verify(passwordEncoder).matches(loginRequest.getPassword(), userDto.getPassword());
        verify(jwtUtil).generateToken(userDto);
    }

    @Test
    public void testLoginUserNotFound() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest("nonexistentuser", "password");

        when(userService.findByUserName(loginRequest.getUsername()))
                .thenReturn(Optional.empty());

        // Act and Assert
        CustomException exception = assertThrows(CustomException.class,
                () -> authService.login(loginRequest));
        assertEquals(BusinessResultEnum.USER_NOT_FOUND, exception.getBusinessResultEnum());

        // Verify that the method was called
        verify(userService).findByUserName(loginRequest.getUsername());
    }

    @Test
    public void testRegisterSuccess() {
        // Arrange
        RegistrationRequest registrationRequest = new RegistrationRequest("newuser", "password");

        when(userService.existsByUsername(registrationRequest.getUsername()))
                .thenReturn(false);
        when(passwordEncoder.encode(registrationRequest.getPassword()))
                .thenReturn("hashedPassword");
        when(userService.save(any(UserEntity.class)))
                .thenReturn(UserDto.builder()
                        .id(1L)
                        .username("newuser")
                        .password("hashedPassword")
                        .build());

        // Act
        RegistrationResponse response = authService.register(registrationRequest);

        // Assert
        assertNotNull(response);
        assertEquals("newuser", response.getUsername());

        // Verify that the methods were called
        verify(userService, times(1)).existsByUsername(registrationRequest.getUsername());
        verify(passwordEncoder, times(1)).encode(registrationRequest.getPassword());
        verify(userService, times(1)).save(any(UserEntity.class));
    }

    @Test
    public void register_when_username_exists_then_throw_ex() {
        // Arrange
        RegistrationRequest registrationRequest = new RegistrationRequest("existinguser", "password");

        when(userService.existsByUsername(registrationRequest.getUsername()))
                .thenReturn(true);

        // Act and Assert
        CustomException exception = assertThrows(CustomException.class,
                () -> authService.register(registrationRequest));
        assertEquals(BusinessResultEnum.USERNAME_EXISTS, exception.getBusinessResultEnum());

        // Verify that the method was called
        verify(userService, times(1)).existsByUsername(registrationRequest.getUsername());
    }

    @Test
    public void login_when_password_not_matched_then_throw_ex() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest("nonexistentuser", "password");
        UserDto userDto = UserDto.builder()
                .password("anyPassword")
                .build();
        when(userService.findByUserName(loginRequest.getUsername()))
                .thenReturn(Optional.of(userDto));
        when(passwordEncoder.matches(loginRequest.getPassword(), userDto.getPassword())).thenReturn(false);


        // Act and Assert
        CustomException exception = assertThrows(CustomException.class,
                () -> authService.login(loginRequest));
        assertEquals(BusinessResultEnum.USERNAME_OR_PASSWORD_INCORRECT, exception.getBusinessResultEnum());


        // Verify that the method was called
        verify(userService, times(1)).findByUserName(loginRequest.getUsername());
        verify(passwordEncoder, times(1))
                .matches(loginRequest.getPassword(), userDto.getPassword());
        verify(jwtUtil, times(0)).generateToken(any(UserDto.class));
    }
    // Similar tests for other scenarios in the register method...
}
