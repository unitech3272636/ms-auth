package az.unitech.msauth.util;

import az.unitech.msauth.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.annotation.PostConstruct;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static az.unitech.msauth.constant.Constants.*;

@Component
@RequiredArgsConstructor
public class JwtUtil {
    @Value("${jwt.private-key}")
    private String strPrivateKey;
    private PrivateKey privateKey;

    @SneakyThrows
    @PostConstruct
    void init() {
        byte[] strPrivateKeyBytes = strPrivateKey.getBytes();
        byte[] decodePrivateKeyBytes = Base64.getDecoder().decode(strPrivateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec =
                new PKCS8EncodedKeySpec(decodePrivateKeyBytes);
        privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
    }

    public String generateToken(UserDto dto) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(USER_ID, dto.getId());
        return doGenerateToken(claims, dto.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.RS512, privateKey).compact();
    }
}
