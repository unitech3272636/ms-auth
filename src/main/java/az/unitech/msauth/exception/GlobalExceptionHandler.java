package az.unitech.msauth.exception;

import az.unitech.msauth.dto.response.CommonErrorResponse;
import az.unitech.msauth.enums.BusinessResultEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public CommonErrorResponse unknownException(Exception exception) {
        exception.printStackTrace();
        return CommonErrorResponse.newInstance(exception.getMessage(), BusinessResultEnum.INTERNAL_ERROR);
    }

    @ExceptionHandler(CustomException.class)
    @ResponseStatus(HttpStatus.OK)
    public CommonErrorResponse unknownException(CustomException exception) {
        return CommonErrorResponse.newInstance(exception.getBusinessResultEnum());
    }
}
