package az.unitech.msauth.enums;

import org.springframework.http.HttpStatus;

public enum BusinessResultEnum {
    ACCESS_TOKEN_EXPIRED(1000, HttpStatus.UNAUTHORIZED, "Access token expired"),
    REFRESH_TOKEN_EXPIRED(1001, HttpStatus.UNAUTHORIZED, "Refresh token expired"),
    USERNAME_OR_PASSWORD_INCORRECT(1002, HttpStatus.UNAUTHORIZED, "The username or password is incorrect"),
    INVALID_TOKEN(1003, HttpStatus.UNAUTHORIZED, "Token is invalid"),
    USER_BLOCKED(1004, HttpStatus.LOCKED, "User is blocked"),
    USERNAME_EXISTS(1005, HttpStatus.BAD_REQUEST, "Username is already exists"),
    USER_NOT_FOUND(1006, HttpStatus.NOT_FOUND, "User not found"),
    ERROR_ON_TOKEN_GENERATE(1007, HttpStatus.CONFLICT, "Token can not be generated"),
    INTERNAL_ERROR(999, HttpStatus.UNAUTHORIZED, "General Error");

    private final Integer code;
    private final HttpStatus httpStatus;
    private final String message;

    BusinessResultEnum(Integer code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }

}
