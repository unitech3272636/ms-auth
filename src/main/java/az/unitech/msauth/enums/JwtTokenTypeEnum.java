package az.unitech.msauth.enums;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.util.function.Function;

public enum JwtTokenTypeEnum {

    ACCESS(username -> {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", "ACCESS");
        return claims;
    }, 3600000, BusinessResultEnum.ACCESS_TOKEN_EXPIRED, "ACCESS"),
    REFRESH(username -> {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", "REFRESH");
        return claims;
    }, 86400000, BusinessResultEnum.REFRESH_TOKEN_EXPIRED, "REFRESH");

    private final Function<String, Claims> createClaims;
    private final long validityTime;
    private BusinessResultEnum businessResultEnum;
    private String roles;

    JwtTokenTypeEnum(Function<String, Claims> createClaims, long validityTime, BusinessResultEnum businessResultEnum, String roles) {
        this.createClaims = createClaims;
        this.validityTime = validityTime;
        this.businessResultEnum = businessResultEnum;
        this.roles = roles;
    }

    public Claims apply(String username) {
        return createClaims.apply(username);
    }

    public long getValidityTime() {
        return validityTime;
    }

    public BusinessResultEnum getBusinessResultEnum() {
        return businessResultEnum;
    }

    public void setBusinessResultEnum(BusinessResultEnum businessResultEnum) {
        this.businessResultEnum = businessResultEnum;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public static JwtTokenTypeEnum getTokenTypeByRoles(String role) {
        for (JwtTokenTypeEnum type : values()) {
            if (type.getRoles().equalsIgnoreCase(role)) {
                return type;
            }
        }
        return ACCESS;
    }
}

