package az.unitech.msauth.controller;

import az.unitech.msauth.dto.request.LoginRequest;
import az.unitech.msauth.dto.request.RegistrationRequest;
import az.unitech.msauth.dto.response.LogInResponse;
import az.unitech.msauth.dto.response.RegistrationResponse;
import az.unitech.msauth.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/login")
    public LogInResponse login(@RequestBody LoginRequest request) {
        return authService.login(request);
    }

    @PostMapping("/register")
    public RegistrationResponse register(@RequestBody RegistrationRequest request) {
        return authService.register(request);
    }
}
