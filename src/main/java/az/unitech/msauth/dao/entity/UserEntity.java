package az.unitech.msauth.dao.entity;

import az.unitech.msauth.dto.request.RegistrationRequest;
import az.unitech.msauth.enums.UserStatusEnum;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username",
            nullable = false)
    private String username;

    @Column(name = "password",
            nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "status",
            nullable = false)
    private UserStatusEnum status;

    public static UserEntityBuilder newInstance(RegistrationRequest request) {
        return UserEntity.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .status(UserStatusEnum.ACTIVE);
    }
}


