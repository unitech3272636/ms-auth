package az.unitech.msauth.constant;

public class Constants {
    public static final String USER_ID = "user_id";
    public static final String ALGORITHM_RSA = "RSA";
    public static final int JWT_TOKEN_VALIDITY = 120 * 60 * 1000;//bunu mutleq azalt !!!!  120 - > 5
}
