package az.unitech.msauth.dto;

import az.unitech.msauth.enums.UserStatusEnum;
import lombok.*;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;
    private String username;
    private String password;
    private UserStatusEnum status;
}
