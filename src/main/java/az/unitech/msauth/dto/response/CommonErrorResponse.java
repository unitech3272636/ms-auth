package az.unitech.msauth.dto.response;

import az.unitech.msauth.enums.BusinessResultEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommonErrorResponse {
    private Long code;
    private String systemMessage;
    private String message;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime time;

    public static CommonErrorResponse newInstance(BusinessResultEnum businessResultEnum) {
        return CommonErrorResponse.builder()
                .code(Long.valueOf(businessResultEnum.getCode()))
                .systemMessage("ERROR")
                .message(businessResultEnum.getMessage())
                .time(LocalDateTime.now())
                .build();
    }

    public static CommonErrorResponse newInstance(String systemMessage, BusinessResultEnum businessResultEnum) {
        return CommonErrorResponse.builder()
                .code(Long.valueOf(businessResultEnum.getCode()))
                .systemMessage(systemMessage)
                .message(businessResultEnum.getMessage())
                .time(LocalDateTime.now())
                .build();
    }
}
