package az.unitech.msauth.service;

import az.unitech.msauth.dto.UserDto;
import az.unitech.msauth.dto.request.LoginRequest;
import az.unitech.msauth.dto.request.RegistrationRequest;
import az.unitech.msauth.dto.response.LogInResponse;
import az.unitech.msauth.dto.response.RegistrationResponse;
import az.unitech.msauth.enums.BusinessResultEnum;
import az.unitech.msauth.exception.CustomException;
import az.unitech.msauth.dao.entity.UserEntity;
import az.unitech.msauth.util.JwtUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;

    public LogInResponse login(LoginRequest request) {
        UserDto userDto = userService.findByUserName(request.getUsername())
                .orElseThrow(() -> CustomException.of(BusinessResultEnum.USER_NOT_FOUND));
        if (!passwordEncoder.matches(request.getPassword(), userDto.getPassword()))
            CustomException.of(BusinessResultEnum.USERNAME_OR_PASSWORD_INCORRECT).throwEx();
        String accessToken = jwtUtil.generateToken(userDto);
        return LogInResponse.builder()
                .accessToken(accessToken)
                .build();
    }

    public RegistrationResponse register(RegistrationRequest request) {
        if (userService.existsByUsername(request.getUsername()))
            CustomException.of(BusinessResultEnum.USERNAME_EXISTS).throwEx();
        String encodePassword = passwordEncoder.encode(request.getPassword());
        UserEntity userEntity = UserEntity.newInstance(request)
                .password(encodePassword)
                .build();
        UserDto savedData = userService.save(userEntity);
        return RegistrationResponse.builder()
                .id(savedData.getId())
                .username(savedData.getUsername())
                .build();
    }
}
