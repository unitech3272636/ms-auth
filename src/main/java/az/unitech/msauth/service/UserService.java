package az.unitech.msauth.service;

import az.unitech.msauth.dto.UserDto;
import az.unitech.msauth.mapping.UserMapping;
import az.unitech.msauth.dao.entity.UserEntity;
import az.unitech.msauth.dao.repo.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapping userMapping;

    public Optional<UserDto> findByUserName(String userName) {
        return userRepository.findByUsername(userName).map(userMapping::toUserDto);
    }

    public boolean existsByUsername(String userName) {
        return userRepository.existsByUsername(userName);
    }

    public UserDto save(UserEntity userEntity) {
        return userMapping.toUserDto(userRepository.save(userEntity));
    }
}
