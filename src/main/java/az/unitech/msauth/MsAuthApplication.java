package az.unitech.msauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

@SpringBootApplication
@EnableEurekaClient
public class MsAuthApplication {

	public static void main(String[] args){
		SpringApplication.run(MsAuthApplication.class, args);
	}

}
