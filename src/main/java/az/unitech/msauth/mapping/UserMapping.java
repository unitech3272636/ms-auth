package az.unitech.msauth.mapping;

import az.unitech.msauth.dao.entity.UserEntity;
import az.unitech.msauth.dto.UserDto;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapping {
    UserDto toUserDto(UserEntity entity);
}
